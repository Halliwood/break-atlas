# break-atlas

#### Introduction
A tool helps you break atlas created by laya.

#### Install

```
npm i break-atlas
```

#### Example

1. Break the specified single atlas
```
break-atlas -s D:/layaproj/bin/res/atlas/myAtlas.atlas
```

2. Break the specified single atlas and specify the output path
```
break-atlas -s D:/layaproj/bin/res/atlas/myAtlas.atlas -o D:/rawpngs
```

3. Break all atlases under the specified source folder
```
break-atlas -s D:/layaproj/bin/res/atlas
```

4. Break all atlases under the current work folder
```
break-atlas
```